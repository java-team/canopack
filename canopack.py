#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import debian.deb822
import re
import fnmatch

PULL = False
PACKAGES = ['jas-plotter', 'freehep-chartableconverter-plugin', 'freehep-util', 'freehep-swing', 'freehep-export', 'freehep-graphics2d', 'freehep-io', 'freehep-graphicsio', 'freehep-xml', 'freehep-graphicsio-tests', 'freehep-graphicsio-svg', 'freehep-graphicsio-pdf', 'freehep-graphicsio-ps', 'freehep-graphicsio-java', 'freehep-graphicsio-emf', 'freehep-graphicsio-swf']
FIX = True
FIX_UNKNOWN_MSG = " > I do not know how to fix this..."

def compare(cond, msg):
    if not cond:
        print(msg)
        if FIX:
            print(FIX_UNKNOWN_MSG)

def compare_field(control, name, value, dry=False):
    if control[name] != value:
        print("{}: {}".format(name, value))
        if FIX and not dry:
            control[name] = value

def compare_deps(control, name, wanted=None, unwanted=None):
    if wanted is None:
        wanted = []
    if unwanted is None:
        unwanted = []
    if name not in control:
        if wanted != []:
            print('{}: missing'.format(name))
            print(FIX_UNKNOWN_MSG)
        return
    deps = [x.strip() for x in control[name].split(',')]
    for d in unwanted:
        deps = [x for x in deps if not fnmatch.fnmatch(x, d)]
    for d in wanted:
        if d not in deps:
            deps.append(d)
    deps.sort()
    if deps == []:
        print('{}: empty'.format(name))
        print(FIX_UNKNOWN_MSG)
    value = "\n " + ",\n ".join(deps)
    if value != control[name]:
        print("{}: {}".format(name, value))
        if FIX:
            control[name] = value

def compare_file(basedir, filename, content, dry=False):
    try:
        with open(os.path.join(basedir, filename)) as fin:
            if fin.read() != content:
                print("{}: {}".format(filename, content.strip().replace('\n', '\\n')))
                if FIX and not dry:
                    with open(os.path.join(basedir, filename), 'w') as fout:
                        fout.write(content)
    except FileNotFoundError:
        print("{}: missing file!".format(filename))
        if FIX:
            print(FIX_UNKNOWN_MSG)

def grep_file(basedir, filename, regex, reverse=False, ok_if_missing=False):
    matcher = re.compile(regex)
    try:
        with open(os.path.join(basedir, filename)) as fin:
            for line in fin:
                if matcher.search(line):
                    if reverse:
                        print("{}: could find {}".format(filename, regex))
                        if FIX:
                            print(FIX_UNKNOWN_MSG)
                    break
            else:
                if not reverse:
                    print("{}: could not find {}".format(filename, regex))
                    if FIX:
                        print(FIX_UNKNOWN_MSG)
    except FileNotFoundError:
        if not ok_if_missing:
            print("{}: missing file!".format(filename))
            if FIX:
                print(FIX_UNKNOWN_MSG)

def check_missing_file(basedir, filename, reverse=False):
    if os.path.exists(os.path.join(basedir, filename)):
        if not reverse:
            print("{}: exists!".format(filename))
            if FIX:
                print(FIX_UNKNOWN_MSG)
    else:
        if reverse:
            print("{}: does not exist!".format(filename))
            if FIX:
                print(FIX_UNKNOWN_MSG)

def sort_elems(value):
    elems = [x.strip() for x in value.split(",")]
    elems.sort()
    return ", ".join(elems)

def sort_and_wrap_elems(value):
    elems = [x.strip() for x in value.split(",")]
    elems.sort()
    return "\n " + ",\n ".join(elems)

def sort_field(control, name, dry=False):
    if name not in control:
        return
    value = sort_elems(control[name])
    if control[name] != value:
        print("{}: {}".format(name, value))
        if FIX and not dry:
            control[name] = value

def sort_and_wrap_field(control, name, dry=False):
    if name not in control:
        return
    value = sort_and_wrap_elems(control[name])
    if control[name] != value:
        print("{}: {}".format(name, value))
        if FIX and not dry:
            control[name] = value

def check_patches(basedir):
    series = os.path.join(basedir, 'debian', 'patches', 'series')
    if os.path.exists(series):
        with open(series) as fseries:
            for line in fseries:
                patchname = os.path.join('debian', 'patches', line.strip())
                with open(os.path.join(basedir, patchname)) as fpatch:
                    content = fpatch.readlines()
                if '---\n' not in content:
                    print("{}: missing DEP-3 separator".format(patchname))
                    if FIX:
                        i = 0
                        while i < len(content):
                            if content[i].startswith('Index: '):
                                break
                            i += 1
                        content = content[:i] + ['---\n', 'This patch header follows DEP-3: http://dep.debian.net/deps/dep3/\n'] + content[i:]
                        with open(os.path.join(basedir, patchname), 'w') as fpatch:
                            fpatch.write("".join(content))


def check_source(control, name):
    compare(list(control.keys()) == ['Source', 'Section', 'Priority', 'Maintainer', 'Uploaders', 'Build-Depends', 'Build-Depends-Indep', 'Standards-Version', 'Vcs-Git', 'Vcs-Browser', 'Homepage'], "Source control fields order")
    compare_field(control, 'Source', name)
    compare_field(control, 'Section', 'java')
    compare_field(control, 'Priority', 'optional')
    compare_field(control, 'Maintainer', 'Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>')
    compare_field(control, 'Uploaders', '''
 Giovanni Mascellani <gio@debian.org>,
 Philipp Huebner <debalance@debian.org>''')
    compare_deps(control, 'Build-Depends', wanted=['debhelper (>= 10)'], unwanted=['debhelper *', 'cdbs', 'quilt'])
    compare_deps(control, 'Build-Depends-Indep')
    compare_field(control, 'Standards-Version', '4.1.1')
    compare_field(control, 'Vcs-Git', 'https://anonscm.debian.org/git/pkg-java/freehep/{}.git'.format(name))
    compare_field(control, 'Vcs-Browser', 'https://anonscm.debian.org/git/pkg-java/freehep/{}.git'.format(name))
    compare_field(control, 'Homepage', 'http://java.freehep.org/', dry=True)

def check_binary(control, name):
    compare(list(control.keys()) == ['Package', 'Architecture', 'Depends', 'Recommends', 'Description'], "Binary control fields order")
    compare_field(control, 'Package', name)
    compare_field(control, 'Architecture', 'all')
    compare_field(control, 'Depends', '${maven:Depends}, ${misc:Depends}')
    compare_field(control, 'Recommends', '${maven:OptionalDepends}')

def check_package(basedir):
    with open(os.path.join(basedir, 'debian', 'control')) as control_file:
        # Checks on the source package
        control_source = debian.deb822.Deb822(control_file)
        name_source = control_source['Source']
        check_source(control_source, name_source)

        # Checks on the binary package
        control_binary = debian.deb822.Deb822(control_file)
        name_binary = control_binary['Package']
        compare(name_binary == 'lib{}-java'.format(name_source), 'Binary package name: lib{}-java'.format(name_source))
        check_binary(control_binary, name_binary)

        # Check there are no other binaries
        compare(dict(debian.deb822.Deb822(control_file)) == {}, 'Additional binary packages')

    # Rewrite file
    if FIX:
        with open(os.path.join(basedir, 'debian', 'control'), 'w') as control_file:
            control_file.write(str(control_source))
            control_file.write('\n')
            control_file.write(str(control_binary))

    check_patches(basedir)

    # Check a few easy files
    compare_file(basedir, 'debian/compat', '10\n')
    compare_file(basedir, 'debian/source/format', '3.0 (quilt)\n')
    compare_file(basedir, 'debian/{}.poms'.format(name_binary), 'pom.xml --no-parent\n')
    compare_file(basedir, 'debian/maven.properties'.format(name_binary), 'maven.test.skip=false\n', dry=True)
    compare_file(basedir, 'debian/gbp.conf',
'''[DEFAULT]
pristine-tar = True
''')

    # Check that dh is used
    #grep_file(basedir, 'debian/rules', 'dh \\$@')
    compare_file(basedir, 'debian/rules',
'''#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

JAVA_HOME := /usr/lib/jvm/default-java

%:
	dh $@
''')

    # Check that topgit was abandoned
    grep_file(basedir, 'debian/patches/series', '-p1', reverse=True, ok_if_missing=True)
    grep_file(basedir, 'debian/patches/series', 'patch/', reverse=True, ok_if_missing=True)
    grep_file(basedir, 'debian/rules', 'topgit', reverse=True)

    # Check there are no boilerplate maven-debian-helper files
    #check_missing_file(basedir, 'debian/maven.ignoreRules')
    #check_missing_file(basedir, 'debian/maven.cleanIgnoreRules')
    #check_missing_file(basedir, 'debian/maven.publishedRules')

    # Check for trivial information in README.source
    grep_file(basedir, 'debian/README.source', 'DEBIANIZATION', reverse=True, ok_if_missing=True)
    grep_file(basedir, 'debian/README.source', 'QUILT', reverse=True, ok_if_missing=True)

    # Check there is a patch for adding classpath to JAR (and no other
    # hacks to do that)
    #check_missing_file(basedir, 'debian/patches/classpath.diff', reverse=True)
    #check_missing_file(basedir, 'debian/{}.classpath'.format(name_binary))

    # TODO: debian/copyright

def main():
    packages = PACKAGES
    if len(sys.argv) > 1:
        packages = sys.argv[1:]
    for package in packages:
        package_dir = os.path.join('packages/{}'.format(package))
        print(' ==> {} <=='.format(package_dir))
        if PULL:
            os.system('cd {} && git checkout master ; git pull --ff-only'.format(package_dir))
        check_package(package_dir)

if __name__ == '__main__':
    main()
